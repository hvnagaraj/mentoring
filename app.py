from flask import Flask
from flask import Flask, request, send_from_directory

app = Flask(__name__, static_url_path='/static')

@app.route('/')
def root():
  return app.send_static_file('index.html')

@app.route("/")
def hello():
    return "Hello World!"

